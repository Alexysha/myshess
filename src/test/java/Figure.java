public abstract class Figure {
    protected Color color;

    protected abstract boolean isRegulation();

    public Color isColor() { return color; }
}

class Horse extends Figure {

    public Horse(Color color) {
        super.color = color;
    }

    @Override
    protected boolean isRegulation() {
        return false;
    }
}

class King extends Figure {

    public King(Color color) {
        super.color = color;
    }

    @Override
    protected boolean isRegulation() {
        return false;
    }
}

class Pawn extends Figure {

    public Pawn(Color color) {
        super.color = color;
    }


    @Override
    protected boolean isRegulation() {
        return false;
    }
}

class Queen extends Figure {

    public Queen(Color color) {
        super.color = color;
    }


    @Override
    protected boolean isRegulation() {
        return false;
    }
}

class Rook extends Figure {

    public Rook(Color color) {
        super.color = color;
    }

    @Override
    protected boolean isRegulation() {
        return false;
    }
}

