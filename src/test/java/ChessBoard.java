abstract public class ChessBoard {
    private Figure[][] chessBoard = new Figure[8][8];

    abstract public boolean setFigureOnBoard(Figure figure, int row, int column);
    abstract public int checkMate();
}



