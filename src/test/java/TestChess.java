import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestChess {
    private ChessBoard chessBoard;
    private Queen queen ;
    private King king ;
    private King kingBlack;
    private Pawn pawn;
    private Horse horse;
    private Rook rook;
    int result;

    @BeforeClass
    public void setUp() {
        queen = new Queen(Color.WHITE);
        king = new King(Color.WHITE);
        pawn = new Pawn(Color.WHITE);
        horse = new Horse(Color.WHITE);
        rook = new Rook(Color.WHITE);
        kingBlack = new King(Color.BLACK);
    }

    @Story("0 фигур на доске")
    @Test
    public void testNotFigureOnBoard() {
        result = chessBoard.checkMate();

        Assert.assertEquals(-666, result);
    }

    @Story("Шах и мат ферзём")
    @Test
    public void testShahAndMate_1() {
        chessBoard.setFigureOnBoard(queen, 1,1);
        chessBoard.setFigureOnBoard(king, 2, 2);
        chessBoard.setFigureOnBoard(kingBlack, 0, 1);

        result = chessBoard.checkMate();

        Assert.assertEquals(result, -1);
    }

    @Story("Шах и мат ферзем (2)")
    @Test
    public void testShahAndMate_3() {
        chessBoard.setFigureOnBoard(kingBlack, 2,7);
        chessBoard.setFigureOnBoard(queen, 2, 6);

        result = chessBoard.checkMate();

        Assert.assertEquals(result, -1);
    }

    @Story("Шах и мат ладьёй")
    @Test
    public void testShahAndMate_2() {
        chessBoard.setFigureOnBoard(kingBlack, 0,3);
        chessBoard.setFigureOnBoard(king, 2, 3);
        chessBoard.setFigureOnBoard(rook, 0, 6);

        result = chessBoard.checkMate();

        Assert.assertEquals(result, -1);
    }

    @Story("Шах пешкой")
    @Test
    public void testShah_1() {
        chessBoard.setFigureOnBoard(pawn, 5,3);
        chessBoard.setFigureOnBoard(king, 4, 4);
        chessBoard.setFigureOnBoard(kingBlack, 4, 2);

        result = chessBoard.checkMate();

        Assert.assertEquals(0, result);
    }

    @Story("Шах конем")
    @Test
    public void testShah_2() {
        chessBoard.setFigureOnBoard(kingBlack, 7,7);
        chessBoard.setFigureOnBoard(horse, 5, 6);

        result = chessBoard.checkMate();

        Assert.assertEquals(0, result);
    }

    @Story("Шах и мат отсутствует")
    @Test
    public void testNotShahAndMat_1() {
        chessBoard.setFigureOnBoard(pawn, 7,7);
        chessBoard.setFigureOnBoard(king, 7, 6);
        chessBoard.setFigureOnBoard(kingBlack, 0, 0);

        result = chessBoard.checkMate();

        Assert.assertEquals(1, result);
    }

    @Story("Шах и мат отсутствует (2)")
    @Test
    public void testNotShahAndMat_2() {
        chessBoard.setFigureOnBoard(horse, 0,0);
        chessBoard.setFigureOnBoard(king, 1, 2);
        chessBoard.setFigureOnBoard(kingBlack, 6, 5);

        result = chessBoard.checkMate();

        Assert.assertEquals(1, result);
    }
}
